# Buildah

> Note: cette image est désormais inutile, merci de favoriser
> `quay.io/buildah/stable`

Construction d'image sans démon docker

## Pourquoi ?
Par exemple construite des images depuis des pods k8s.

## Référence
- https://buildah.io
- https://www.projectatomic.io/blog/2018/03/building-buildah-container-image-for-kubernetes/

## Dépôt
- https://github.com/containers/buildah

## Documentation
- [buildah](https://www.mankier.com/1/buildah)
- [buildah login](https://www.mankier.com/1/buildah-login)
- [buildah logout](https://www.mankier.com/1/buildah-logout)
- [buildah build-using-dockerfile](https://www.mankier.com/1/buildah-bud)
- [buildah push](https://www.mankier.com/1/buildah-push)

## Usage

```yaml
image: gricad-registry.univ-grenoble-alpes.fr/kubernetes-alpes/buildah:latest

variables:
  REGISTRY_LOGIN: buildah login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD
  REGISTRY_LOGOUT: buildah logout
  IMAGE_BUILD: buildah build-using-dockerfile --storage-driver vfs --format docker --file Dockerfile --tag
  IMAGE_PUSH: buildah push --storage-driver vfs

before_script:
- $REGISTRY_LOGIN $CI_REGISTRY

after_script:
- $REGISTRY_LOGOUT $CI_REGISTRY

build:
  stage: build
  script:
    - $IMAGE_BUILD $CI_REGISTRY_IMAGE .
    - $IMAGE_PUSH  $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE
```
