FROM fedora:34
RUN dnf -y update \
 && dnf -y install buildah runc \
 && sed -i 's/short-name-mode="enforcing"/short-name-mode="permissive"/g' /etc/containers/registries.conf
